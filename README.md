Dockerfile to build a shiv Docker image
=======================================

Shiv files are similar to PEX files in that are self-contained
executable Python virtual environments, however they are more
efficient and require less resources to run.

Shiv uses Python3 exclusively.

Building
--------

This image is built automatically by GitLab.

How to use this image
---------------------

For example to create the awx CLI utility from the awxkit package:

    $ docker run --rm -v $(pwd):/build registry.esss.lu.se/ics-docker/shiv:latest shiv -c awx -o awx awxkit


For more documentation - https://shiv.readthedocs.io
