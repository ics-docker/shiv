FROM centos:7.8.2003

LABEL MAINTAINER "amir.forsat@ess.eu"

ENV SHIV_VERSION 0.4.0

# Install common dependencies
RUN  yum -y update \
  && yum -y install \
     gcc \
     make \
     python3-devel \
     python3-setuptools \
     python3-pip \
  && yum clean all

ENV LC_ALL=en_US.utf-8 \
    LANG=en_US.utf-8

WORKDIR /build

# Install shiv
RUN python3 -m venv shiv \
  && source shiv/bin/activate \
  && pip install --no-cache-dir --upgrade pip setuptools \
  && pip install --no-cache-dir shiv==$SHIV_VERSION \
  && shiv -c shiv -o /usr/local/bin/shiv shiv \
  && deactivate \
  && rm -rf shiv
